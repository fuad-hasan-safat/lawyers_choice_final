package com.safati.lawyerschoice;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Details extends AppCompatActivity {
    private Button btn;
    TextView letterNoView, dateView, txt, txt1, txt2;
    String str,str1,str2,str3;
    private static final int STORAGE_CODES = 1000;



    //private AppCompatEditText letterNoInput, dateInput, bankNameInput, bankAddressInput,branchNameInput,clientNameInput;
    //String stringName[] = new String[6];
   public String letterNoStore, dateStore, bankNameStore, bankAddressStore,branchNameStore,clientNameStore,areaStore,receiptNoStore;
    //TextView letterNoView, dateView, bankNameView, bankAddressView,branchNameView,clientNameView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        btn = findViewById(R.id.makePdfID);



        setAllValue();  // .........................................

        show();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M){
                    if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
                    {
                        String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        requestPermissions(permission,STORAGE_CODES);

                    }
                    else
                    {
                       savePDF();
                    }
                }
                else
                {
                    savePDF();
                }
            }
        });



    }

    private void setAllValue() {
         letterNoView =  findViewById(R.id.letterNoId2);
         dateView =  findViewById(R.id.dateId2);
         txt = findViewById(R.id.textViewId2);
         txt1 = findViewById(R.id.textViewId3);
         txt2 = findViewById(R.id.textViewId4);

        Intent intent = getIntent();

        letterNoStore = intent.getStringExtra("letterNoStore");
        dateStore =  intent.getStringExtra("dateStore");

        bankNameStore = intent.getStringExtra("bankNameStore");
        bankAddressStore =  intent.getStringExtra("bankAddressStore");

        branchNameStore = intent.getStringExtra("branchNameStore");
        clientNameStore= intent.getStringExtra("clientNameStore");

        areaStore = intent.getStringExtra("areaStore");
        receiptNoStore = intent.getStringExtra("receiptNoStore");

    }

    private void savePDF() {
        Document mDoc = new Document();

        String mFileName = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(System.currentTimeMillis());
        String  mFilePath = Environment.getExternalStorageDirectory() + "/" + mFileName +".pdf";

        try{
            PdfWriter.getInstance(mDoc, new FileOutputStream(mFilePath));
            mDoc.open();
          //  String mText = editText.getText().toString();

            mDoc.addAuthor("Fuad Hasan");

            mDoc.add(new Paragraph(str));
            mDoc.addHeader("Legal Opinion","inline");
            mDoc.add(new Paragraph(str2));
            mDoc.add(new Paragraph(str3));

            mDoc.close();

            Toast.makeText(this,mFileName+".pdf \nis saved to\n"+ mFilePath,Toast.LENGTH_SHORT).show();

        }catch (Exception ex){
            Toast.makeText(this, ex.getMessage(),Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case STORAGE_CODES:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    savePDF();
                }else {
                    Toast.makeText(this,"Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }





    public void show() {



        System.out.println("latterno = " +letterNoStore + "  Date: "+dateStore);

        /*------------------------------------------------------------------------------------------------------------------*/

        String line = "_________________________________________________________\n";
        /*-------------------------------------------------------------------------------------------------------------------*/

        String subject = "Subject: Legal opinion upon vetting of the property documents standing in the name of " + clientNameStore + "; A/c. " + clientNameStore+".";
         str = "To" + "\n" + "Head of the Branch" + "\n" + bankNameStore+ ",\n"  + branchNameStore + "\n" + bankAddressStore + "\n\n" + subject +"\n\n" + "Dear Sir," + "\n" +
                "In reference to your latter no. " + letterNoStore + " dated " + dateStore +
                " regarding the above captioned subject and as well as our previous correspondence with you; the Bank has forwarded us deeds & documents on account of " +
                clientNameStore + " for vetting and legal opinion from the Chamber's end." + "\n\n" +
                "Upon perusal of the referred deeds & documents, a complete opinion on account of the captioned client of the Bank has been prepared in a manner as follows:" +"\n\n\n\n\n"

                ;
        // TextView bankNameView =

        String areaShow = "\n" +line + "\n1. Area: " + "Total land " + areaStore + " decimals.\n" + line;

        String biaShow = "\n2. Bia Deed(s): " + "N/A.\n" + line;

        String paymentReceipt = "\n3. Ground Rent Payment Receipt(s): " + "Submitted ground rent payment receipt no. " + receiptNoStore + " in the name of "
                + clientNameStore +" for land area " + areaStore +" decimals.\n" + line;

        String necShow = "\n4. Non-Encumbrance Certificate (NEC): " + "Not Submitted\n" + line;

        String taxPaymentShow = "\n5. City Corporation Holding Tax Payment Receipt (If applicable): " + "N/A\n" + line;

        String otherPapersShow = "\n6. Other Papers/ Document: " + "N/A\n" + line;

        String requiredPaperShow = "\n7. Required papers/ documents: " + "\n\na. SRO token. \n" +
                "b. Up to date Ground rent in the name of present owner.\n" +
                "c. NID\n" + line;



         str2 = "\n8. Opinion:\nIn view of the above, we are of opinion that " + clientNameStore + " acquired the right title" +
                "& ownership over the scheduled property." +"\n\n"

                + " However, the Bank may accept the property as security of loan subject to only procuring all" +
                "submitted papers documents as original copies instead of photocopies prior to creation of" +
                "mortgage over the scheduled land.\n\n" +
                "In addition, the Bank, before taking mortgage on the property in question, should verify the" +
                "spot physically for ascertaining actual and peaceful possession of the landowner over the" +
                "same.\n\n" +
                "For this opinion, we have assumed that:\n" +
                "All documents/papers submitted to us as photocopies are complete and conform to" +
                "authentic original documents.\n" +
                "All representations and statements made with regard to the documents by the parties are" +
                "true and accurate.\n" + line
                ;

        str2 = areaShow + biaShow + paymentReceipt + necShow + taxPaymentShow + otherPapersShow + requiredPaperShow + str2 ;


         str3 =  "\nThe file along with all photocopies of the property related papers and documents are retained" +
                "here in chamber. This opinion has been given as per submitted papers (photocopy). Should" +
                "you have any further queries in this regard please do not hesitate to contact us."
                +"\n\n\n\n"
                +"Thanking you.\n"
                +"Yours sincerely,"
                +"\n\n\n\n"
                +"(Syed Sanaul Haque)\n"
                +"Advocate,Supreme Court of Bangladesh\n"
                +"For: HAQUE & ASSOCIATES\n"
                ;



       letterNoView.setText(letterNoStore);
        dateView.setText(dateStore);
        txt.setText(str);
        txt1.setText(str2);
        txt2.setText(str3);

      //  String p = letterNoStore + dateStore + str + str2 + str3;

        savePDF();


       }

    }


